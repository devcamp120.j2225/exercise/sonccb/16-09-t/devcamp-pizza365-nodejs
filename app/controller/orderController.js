// import mongoose
const mongoose = require('mongoose');
// import modal
const orderModel = require('../model/orderModel');
const userModel = require('../model/userModel');
const drinkModel = require('../model/drinkModel');
const voucherModel = require('../model/voucherModel');
// create new order
const createNewOrder = (request, response) => {
    //b1 thu thập dữ liệu
    let body = request.body;
    //b2 sử dụng cơ sở dữ liệu
    //tìm kiếm xem email user có tồn tại không
    userModel.findOne({
        email: request.query.email
    }, (errorFindUser, userExist) => {
        // nếu xảy ra lỗi thì trả về lỗi 500
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            // nếu không tồn tại thì tạo user mới và order mới
            if (!userExist) {
                // tạo user mới
                userModel.create(
                    {
                        _id: mongoose.Types.ObjectId(),
                        hoTen: body.hoTen,
                        email: body.email,
                        diaChi: body.diaChi,
                        soDienThoai: body.soDienThoai,
                    }, (error, userData) => {
                        if (error) {
                            response.status(500).json({
                                status: "Error 500: Internal sever Error",
                                message: error.message
                            })
                        } else {
                            orderModel.create(
                                {
                                    _id: mongoose.Types.ObjectId(),
                                    orderCode: Math.random().toString(36).substring(1, 8),
                                    hoTen: userData.hoTen,
                                    email: userData.email,
                                    diaChi: userData.diaChi,
                                    soDienThoai: userData.soDienThoai,
                                    loiNhan: body.loiNhan,
                                    kichCo: body.kichCo,
                                    duongKinh: body.duongKinh,
                                    suon: body.suon,
                                    salad: body.salad,
                                    loaiPizza: body.loaiPizza,
                                    maVoucher: body.maVoucher,
                                    thanhTien: body.thanhTien,
                                    giamGia: body.giamGia,
                                    maNuocUong: body.maNuocUong,
                                    soLuongNuoc: body.soLuongNuoc,
                                    trangThai: body.trangThai,
                                }, (error, orderData) => {
                                    if (error) {
                                        response.status(500).json({
                                            status: "Error 500: Internal sever Error",
                                            message: error.message
                                        })
                                    } else {
                                        console.log(orderData);
                                        userModel.findByIdAndUpdate(userData._id, { $push: { orders: orderData._id } }, (error, orderCreate) => {
                                            console.log(orderData);
                                            response.status(200).json({
                                                status: "Success: create orders success",
                                                data: {
                                                    _id: userData._id,
                                                    orderCode: orderData.orderCode,
                                                    kichCo: orderData.kichCo,
                                                    duongKinh: orderData.duongKinh,
                                                    suon: orderData.suon,
                                                    salad: orderData.salad,
                                                    loaiPizza: orderData.loaiPizza,
                                                    maVoucher: orderData.maVoucher,
                                                    thanhTien: orderData.thanhTien,
                                                    giamGia: orderData.giamGia,
                                                    maNuocUong: orderData.maNuocUong,
                                                    soLuongNuoc: orderData.soLuongNuoc,
                                                    hoTen: userData.hoTen,
                                                    email: userData.email,
                                                    diaChi: userData.diaChi,
                                                    soDienThoai: userData.soDienThoai,
                                                    loiNhan: orderData.loiNhan,
                                                    trangThai: orderData.trangThai,
                                                }
                                            })
                                        }).populate('orders')
                                    }
                                })
                        }
                    })
            } else {
                orderModel.create({
                    _id: mongoose.Types.ObjectId(),
                    orderCode: Math.random().toString(36).substring(1, 8),
                    hoTen: userExist.hoTen,
                    email: userExist.email,
                    diaChi: userExist.diaChi,
                    soDienThoai: userExist.soDienThoai,
                    kichCo: body.kichCo,
                    duongKinh: body.duongKinh,
                    suon: body.suon,
                    salad: body.salad,
                    loaiPizza: body.loaiPizza,
                    maVoucher: body.maVoucher,
                    giamGia: body.giamGia,
                    thanhTien: body.thanhTien,
                    maNuocUong: body.maNuocUong,
                    soLuongNuoc: body.soLuongNuoc,
                    loiNhan: body.loiNhan,
                    trangThai: body.trangThai,
                }, (error, orderData) => {
                    if (error) {
                        response.status(500).json({
                            status: "Error 500: Internal sever Error",
                            message: error.message
                        })
                    } else {
                        userModel.findByIdAndUpdate(userExist._id, { $push: { orders: orderData._id } }, (error, userData) => {
                            console.log(orderData);
                            response.status(200).json({
                                status: "Success: create orders success",
                                data: {
                                    _id: userExist._id,
                                    orderCode: orderData.orderCode,
                                    kichCo: orderData.kichCo,
                                    duongKinh: orderData.duongKinh,
                                    suon: orderData.suon,
                                    salad: orderData.salad,
                                    loaiPizza: orderData.loaiPizza,
                                    maVoucher: orderData.maVoucher,
                                    thanhTien: orderData.thanhTien,
                                    giamGia: orderData.giamGia,
                                    maNuocUong: orderData.maNuocUong,
                                    soLuongNuoc: orderData.soLuongNuoc,
                                    hoTen: userExist.hoTen,
                                    email: userExist.email,
                                    diaChi: userExist.diaChi,
                                    soDienThoai: userExist.soDienThoai,
                                    loiNhan: orderData.loiNhan,
                                    trangThai: orderData.trangThai,
                                }
                            })
                        }).populate('orders')
                    }
                })
            }
        }
    })
}




// get all orders
const getAllOrder = (request, response) => {

    //b3: thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get all orders success",
                data: data
            })
        }
    })
}

const getAllOrderOfUser = (request, response) => {

    let userId = request.params.userid;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return req.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    numberOrders: data.orders.length,
                    order: data
                })
            }
        })
}
// get order by id
const getOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "order ID is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get order by id success",
                data: data
            })
        }
    })
}

//update order theo id
const updateOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let body = request.body;
    //b2: thu thập dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let orderUpdate = {
        kichCo: body.kichCo,
        duongKinh: body.duongKinh,
        suon: body.suon,
        salad: body.salad,
        loaiPizza: body.loaiPizza,
        maVoucher: body.maVoucher,
        thanhTien: body.thanhTien,
        giamGia: body.giamGia,
        maNuocUong: body.maNuocUong,
        soLuongNuoc: body.soLuongNuoc,
        hoTen: body.hoTen,
        email: body.email,
        diaChi: body.diaChi,
        soDienThoai: body.soDienThoai,
        loiNhan: body.loiNhan,
        trangThai: body.trangThai,
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}
// xóa user dựa vào id
const deleteOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order Id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete order success"
            })
        }
    })
}

// export controller methods
module.exports = {
    createNewOrder: createNewOrder,
    getAllOrder: getAllOrder,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
    getAllOrderOfUser
}