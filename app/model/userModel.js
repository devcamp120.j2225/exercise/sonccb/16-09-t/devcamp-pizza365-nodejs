// bước 1: import mongoose
const mongoose = require('mongoose');

// bước 2: khai báo schema từ thư viện mongoose
const Schema = mongoose.Schema;

// bước 3: khởi tạo 1 schema với các thuộc tính được yêu cầu
const userSchema = new Schema({
    // _id: {
    //     type: mongoose.Types.ObjectId,
    // },
    hoTen: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,

    },
    diaChi: {
        type: String,
        required: true,
    },
    soDienThoai: {
        type: String,
        required: true,
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "orders"
        }
    ],
},{
    timestamps: true
})
module.exports = mongoose.model("user", userSchema)