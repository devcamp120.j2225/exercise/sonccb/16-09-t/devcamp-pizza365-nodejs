// khởi tạo bộ thư viện
const express = require('express');

// import drink controller
const { createDrink, getAllDrink, getDrinkByModel ,getDrinkById, updateDrinkById, deleteDrinkById } = require('../controller/drinkController')

//khởi tạo router
const router = express.Router();

router.get("/drinks", getAllDrink)

router.post("/drinks", createDrink)

router.get("/drinks/:drinkId", getDrinkById)

router.put("/drinks/:drinkId", updateDrinkById)

router.delete("/drinks/:drinkId", deleteDrinkById)

router.get('/devcamp-pizza365/drinks', getDrinkByModel);

// export dữ liệu thành 1 module
module.exports = router;