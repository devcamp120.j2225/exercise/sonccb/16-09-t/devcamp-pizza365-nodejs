// khởi tạo bộ thư viện
const express = require('express');
// import module
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controller/userController')
//khởi tạo router
const router = express.Router();
// create new user
router.post("/users", createUser);
// get all user
router.get("/users", getAllUser)
// get user by id
router.get("/users/:userId", getUserById)
// get user update by id
router.put("/users/:userId", updateUserById)
// delete user
router.delete("/users/:userId", deleteUserById)

// export dữ liệu thành 1 module
module.exports = router;